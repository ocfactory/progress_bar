<?php
// Heading
$_['heading_title']    = 'Progress Bar';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Progress Bar module!';
$_['text_edit']        = 'Edit Progress Bar Module';

// Entry
$_['entry_script_path']= 'Script Path';
$_['entry_style_path'] = 'Style Path';
$_['entry_status']     = 'Status';
$_['entry_theme']      = 'Themes';
$_['entry_color']      = 'Colors';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify featured module!';
$_['error_script_path']= 'Script path must be filled!';
$_['error_style_path'] = 'Style path must be filled!';