<?php
class ControllerModuleProgress extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/progress');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('extension/module');

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('progress', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('module/progress', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_script_path'] = $this->language->get('entry_script_path');
		$data['entry_style_path'] = $this->language->get('entry_style_path');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_theme'] = $this->language->get('entry_theme');
		$data['entry_color'] = $this->language->get('entry_color');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);


		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/progress', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/progress', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['token'] = $this->session->data['token'];

		$colors = array_slice(scandir(DIR_CATALOG . 'view/javascript/pace/themes'), 2);
		foreach ($colors as $key => $value) {
			$data['colors'][$key] = $value;
		}

		$themes = array('minimal' => 'minimal', 'flash' => 'flash','barber shop' => 'barber-shop','mac osx' => 'mac-osx','fill left' => 'fill-left','flat top' => 'flat-top','big counter' => 'big-counter','corner indicator' => 'corner-indicator','bounce' => 'bounce','loading bar' => 'loading-bar','center circle' => 'center-circle','center atom' => 'center-atom','center radar' => 'center-radar','center simple' => 'center-simple');
		foreach ($themes as $key => $value) {
			$data['themes'][$key] = $value;
		}

		if (isset($this->request->post['progress_script'])) {
			$data['progress_script'] = $this->request->post['progress_script'];
		} elseif ($this->config->get('progress_script')) {
			$data['progress_script'] = $this->config->get('progress_script');
		} else {
			$data['progress_script'] = '';
		}

		if (isset($this->request->post['progress_color'])) {
			$data['progress_color'] = $this->request->post['progress_color'];
		} elseif ($this->config->get('progress_color')) {
			$data['progress_color'] = $this->config->get('progress_color');
		} else {
			$data['progress_color'] = '';
		}

		if (isset($this->request->post['progress_theme'])) {
			$data['progress_theme'] = $this->request->post['progress_theme'];
		} elseif ($this->config->get('progress_theme')) {
			$data['progress_theme'] = $this->config->get('progress_theme');
		} else {
			$data['progress_theme'] = '';
		}

		if (isset($this->request->post['progress_style'])) {
			$data['progress_style'] = 'pace-theme-' . $this->request->post['progress_theme'] . '.css';
		} elseif ($this->config->get('progress_style')) {
			$data['progress_style'] = $this->config->get('progress_style');
		}  else {
			$data['progress_style'] = '';
		}
		
		if (isset($this->request->post['progress_status'])) {
			$data['progress_status'] = $this->request->post['progress_status'];
		} elseif ($this->config->get('progress_status')) {
			$data['progress_status'] = $this->config->get('progress_status');
		}  else {
			$data['progress_status'] = '';
		}
				
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/progress.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/progress')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}
}